﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEmployee
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateTOEntre = new DateTime(2009, 8, 1);
            Ouvrier testO = new Ouvrier(1, "Esslinger", "Nathan", "1997", dateTOEntre);
            Cadre testC = new Cadre(2, "Saiko", "Mahomadou", "1994", 4);
            Patron testP = new Patron(3, "Osez", "Jose", "2015", 147852, 10);
            Console.WriteLine(testO.ToString());
            Console.WriteLine(testC.ToString());
            Console.WriteLine(testP.ToString());
            Console.ReadLine();
        }
    }
}
