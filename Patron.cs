﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEmployee
{
    class Patron : Employe
    {
        public Patron(int matricule, string nom, string prenom, string dateDeNaissance, int cA, int pourcent) : base(matricule, nom, prenom, dateDeNaissance)
        {
            this.CA = cA;
            this.Pourcent = pourcent;
        }
        public int CA { get; set; }
        public int Pourcent { get; set; }
        public int SalaireP;
        public override int GetSalaire()
        {
            SalaireP = CA * Pourcent / 100;
            return SalaireP;
        }
        public override string ToString()
        {
            return base.ToString()+ " Salaire : " + GetSalaire();
        }
    }
}
