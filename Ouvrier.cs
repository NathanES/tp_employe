﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEmployee
{
    class Ouvrier : Employe
    {
        public int anciennete;
        public int salaireFinal;
        public int salaireBase = 2500;
        public Ouvrier(int matricule, string nom, string prenom, string dateDeNaissance, DateTime dateDEntrer ) : base(matricule,nom,prenom,dateDeNaissance)
        {
            this.DateDEntrer = dateDEntrer;
        }
        public DateTime DateDEntrer { get; set; }
        public override int GetSalaire()
        {
            //calcul de l'anciennete
            this.anciennete = DateTime.Compare(DateDEntrer,Today);
            this.salaireFinal = this.salaireBase + (this.anciennete*10) * 100;//this.anciennete * 10 parce que un decallage de 10 est egale a 1

            if(this.salaireFinal> (2*this.salaireBase))
            {
                return 2 * this.salaireBase;
            }
            else
            {
                return this.salaireFinal;
            }
        }
        public override string ToString()
        {
            return base.ToString() + " Salaire : " + GetSalaire();
        }
    }
}
