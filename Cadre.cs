﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEmployee
{
    class Cadre : Employe
    {
        public int salaireC;
        public Cadre(int matricule, string nom, string prenom, string dateDeNaissance, int indice) : base(matricule, nom, prenom, dateDeNaissance)
        {
            this.Indice = indice;
        }
        public int Indice { get; set; }
        public override int GetSalaire()
        {
            switch(Indice)
            {
                case 1: salaireC = 13000;
                    break;
                case 2: salaireC = 15000;
                    break;
                case 3: salaireC = 17000;
                    break;
                case 4: salaireC = 20000;
                    break;
                default: Console.WriteLine("Erreur d'indice");
                    break;
            }
            return salaireC;
        }
        public override string ToString()
        {
            return base.ToString() + " Salaire : " + GetSalaire() ;
        }
    }
}
