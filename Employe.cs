﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEmployee
{
    abstract class Employe
    {
        public int salaire;
        public static DateTime Today { get; }
        public int Matricule { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string DateDeNaissance { get; set; }
        public Employe(int matricule, string nom, string prenom, string dateDeNaissance)
        {
            this.Matricule = matricule;
            this.Nom = nom;
            this.Prenom = prenom;
            this.DateDeNaissance = dateDeNaissance;
        }
        public override string ToString()
        {
            string detail;
            detail = Matricule + " " + Nom + " " + Prenom + " " + DateDeNaissance + " ";
            return detail;
        }
        public virtual int GetSalaire()
        {
  
            return salaire;
        }
    }
}
